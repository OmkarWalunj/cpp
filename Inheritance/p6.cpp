// inheritance

#include<iostream>

class Parent{
	public:

	int x=30;

	Parent(){
		std::cout<<"Parent Constructor"<<std::endl;
	}

	void getdata(){
		std::cout<<"Parent x:"<<x<<std::endl;
	}

};

class Child:public Parent{

	public:
		Child(){
			std::cout<<"Child Constructor"<<std::endl;
		}
		void getdata(){
			Parent::getdata();
			std::cout<<"Child x:"<<x<<std::endl;
		}

};

int main(){
	Child obj;

	obj.getdata();
	obj.Parent::getdata();
	(Parent (obj)).getdata();

	return 0;
}
