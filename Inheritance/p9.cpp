//Multiple Inheritance
#include <iostream>

class Parent1{

	public :
		Parent1(){
			std::cout<<"Constructor Parent-1"<<std::endl;
		}
};
class Parent2{

	public :
		Parent2(){
			std::cout<<"Constructor Parent-2"<<std::endl;
		}
};
class Child:public Parent1,public Parent2{

	public :
		Child(){
			std::cout<<"Constructor Child"<<std::endl;
		}
};

int main(){

	Child obj;
	return 0;
}
/*
 * COnstructor Parent-1
 * Constructor Parent-2
 * Constructor Child
 */
