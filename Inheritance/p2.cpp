#include <iostream>

class Parent{

	public:
		Parent(){
			std::cout<<"Parent constructor"<<std::endl;
		}
		~Parent(){

			std::cout<<"Notify Parent Destructor"<<std::endl;
		}
};
class Child : public Parent{

	public:
		Child(){
			std::cout<<"Child Constructor"<<std::endl;
		}
		~Child(){
			std::cout<<"Notify Child Destructor"<<std::endl;
		}
};

int main(){
	Child obj;
	
	Child* obj1=new Child();
	delete obj1;
	return 0;

}
/*parent constructor
 * child costructor
 * parent constructor 
 * child constructor
 * notify child Destructor
 * notify parent Destructor
 * notify child Destrutor
 * notify parent Destructor
 *
 */
