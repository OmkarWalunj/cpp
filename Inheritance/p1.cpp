#include <iostream>

class Employee{

	std::string ename="Kanaha";
	int empid=255;

	public:
		Employee(){
			std::cout<<"Employee constructor"<<std::endl;
		}
		Employee(std::string ename,int empid){
			
			std::cout<<"Employee para constructor"<<std::endl;
			
			this->ename=ename;
			this->empid=empid;
			
		}
		void setter(std::string ename,int empid){
			this->ename=ename;
			this->empid=empid;
		}
		void getData(){
			std::cout<<"Employee Name: "<<ename<<"\n"<<"empid: "<<empid<<std::endl;
		}
		~Employee(){

			std::cout<<"Notify Employee Destructor"<<std::endl;
		}
};
class Company{

	std::string cname=" ";
	int strEmp=1000;
	Employee obj;
	public:
		Company(std::string cname,int strEmp){
			std::cout<<"Company Constructor"<<std::endl;
			Employee("Rahul",500); //  create new object
			this->cname=cname;
			this->strEmp=strEmp;
			obj.setter("Omkar",200);


		}
		void getInfo(){

			std::cout<<"Company Name: "<<cname<<"\n"<<"EMployee: "<<strEmp<<std::endl;
			obj.getData();
		}
		~Company(){
			std::cout<<"Notify Company Destructor"<<std::endl;
		}
};

int main(){
	Company obj("Ptc",5000);
	obj.getInfo();

	return 0;

}
/*Employee constructor  -creating Emplyee object in company
 * Company Constructor   -call company constructor
 * Employee para constructor  -call Emplyee para const create new object
 * Notify Employee Destructor  -end scope of new object so call destructor
 * Company Name: Ptc    
 * EMployee: 5000
 * Employee Name: Omkar
 * empid: 200
 * Notify Company Destructor  end scope of company obj
 * Notify Employee Destructor call to emplyee destructor delete obj
 */
