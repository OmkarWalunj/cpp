//private inheritance

#include<iostream>

class Parent{

	int x=10;

	protected:
	int y=20;

	public:

	int z=30;

	Parent(){
		std::cout<<"Parent Constructor"<<std::endl;
	}

};

class Child:private Parent{

	public:
		Child(){
			std::cout<<"Child Constructor"<<std::endl;
		}

};

int main(){
	Child obj;

	std::cout<<obj.x<<obj.y<<obj.z<<std::endl;  //private(x), protected(y) and inaccesable (z)with in contextext

	return 0;
}
