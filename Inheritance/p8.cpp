#include <iostream>

class Parent{

	int x=10;
	protected:
	int y=20;
	public:
	int z=30;
	void getdata(){
		std::cout<<"In getdata"<<std::endl;
	}
};

class Child:public Parent{

//	using Parent::getdata;
	public:
		using Parent::y;
		using Parent::getdata;
	void getdata()=delete;
};

int main(){

	Child obj;
	std::cout<<obj.y<<obj.z<<std::endl;
	obj.getdata();
	return 0;
}
