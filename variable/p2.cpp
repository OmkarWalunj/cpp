#include <iostream>
int main(){
	int x=10;  //copy/value Initialization
	int y=20.5f;

	std:: cout<<x<<std::endl;  //10
	std:: cout<<y<<std::endl;  //20	
	
	int a{10};
//	int b{2.5f};

	std:: cout<<a<<std::endl;  //10
//      std:: cout<<b<<std::endl;  //error:narrowing conversionof 1 use from float to int
	
	[[maybe_unuse]]int c{20};  // Warning:may be unuse attribute ignore(version 17)

	std:: cout<<c<<std::endl; //20
	return 0;
}
