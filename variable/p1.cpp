#include <iostream>
int main(){
	int x=10;  //copy/value Initialization
	int y(20);  //direct Initialization
	int z{30};   //uniform Initialization(version 11)
	std:: cout<<x<<std::endl;
	std:: cout<<y<<std::endl;	
	std:: cout<<z<<std::endl;

	return 0;
}
