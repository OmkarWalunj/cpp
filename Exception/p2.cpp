#include <iostream>

class AboveAvgSalExcep{

	std::string excep;

	public:
		AboveAvgSalExcep(std::string excep){

			this->excep=excep;
		}
		std::string getException(){
			return excep;
		}
};

class BelowAvgSalExcep{

	std::string excep;

	public:
		BelowAvgSalExcep(std::string excep){

			this->excep=excep;
		}
		std::string getException(){
			return excep;
		}
};
class Employee{
	std::string ename;
	float sal;
	public :
		Employee(std::string ename,float sal){

			this->ename=ename;
			this->sal=sal;
		}

		float getsal(){

			return sal;
		}
		std::string getname(){
			return ename;
		}
};
class Demo{
	public:
	void check(Employee obj[]){

		for(int i=0;i<5;i++){
			if(obj[i].getsal() >1500000 )
				throw AboveAvgSalExcep("Above salary");
			if(obj[i].getsal() <500000){
				throw BelowAvgSalExcep("Below salary");
			}else{
				std::cout<<obj[i].getname()<<std::endl;
				std::cout<<obj[i].getsal()<<std::endl;
			}
		}
	}
};

int main(){

	Employee obj("Omkar",500000);
	Employee obj1("Aditya",1600000);
	Employee obj2("Aniket",200000);
	Employee obj3("VIshal",1000000);
	Employee obj4("Tejas",800000);

	Employee obj5[]={obj,obj1,obj2,obj3,obj4};
	Demo obj6;

	try{
		obj6.check(obj5);
	}catch(AboveAvgSalExcep& objj){

		std::cout<<"Exception Occured "<<objj.getException()<<std::endl;
	}catch(BelowAvgSalExcep& objjj){
		std::cout<<"Exception Occured "<<objjj.getException()<<std::endl;
	}
	return 0;
}

