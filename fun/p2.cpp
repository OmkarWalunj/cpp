//inline function

#include <iostream>
#define mult(x,y) x*y
inline int sum(int x,int y){

	return x+y;
}

int main(){

	int x=10;
	int y=20;

	std::cout<< sum(x,y)<<std::endl;
	std::cout<< mult(x,y)<<std::endl;

	return 0;
}
