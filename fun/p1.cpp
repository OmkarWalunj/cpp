// constant in function
#include <iostream>

int main(){
	int y;
	std::cout<<"Enter Value:"<<std::endl;
	std::cin>>y;

	const int x=y;
	std::cout<<x<<std::endl; 
	
	//x=30;  //error:read-only variable
	const int * ptr=&x;  //data constant
	int z=30;
	ptr=&z;  //no error
	
	int const*ptr1=&x;  //data constant
	int * const ptr2=&x; //pointer constant
	ptr2=&z;  //error		    

	return 0;
}
